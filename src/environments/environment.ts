// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAIRroXYcdhQfawH_A3YiNHcSiBDfAjrVI",
    authDomain: "pathfinder-gm-tools.firebaseapp.com",
    databaseURL: "https://pathfinder-gm-tools.firebaseio.com",
    projectId: "pathfinder-gm-tools",
    storageBucket: "pathfinder-gm-tools.appspot.com",
    messagingSenderId: "1099142712002"
  },
};

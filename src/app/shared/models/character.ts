export class Character {
    name: string;
    type: string;
    initiative: number;
}
import { Character } from '../models/character';

export const CHARACTERS: Character[] = [
    { name: 'Eldane', type: 'pc', initiative: 22 },
    { name: 'Vincent', type: 'pc', initiative: 19 },
    { name: 'Khaleesi', type: 'pc', initiative: 7 },
    { name: 'Klodsmajor', type: 'pc', initiative: 5 },
    { name: 'Niidriël', type: 'pc', initiative: 12 },
];

export const ENCOUNTER_CHARACTERS: Character[] = [
    { name: 'Erin Habe', type: 'pc', initiative: 6 },
    { name: 'Grunt 1', type: 'pc', initiative: 13 },
    { name: 'Grunt 2', type: 'pc', initiative: 20 },
];
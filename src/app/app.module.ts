import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { environment } from './../environments/environment';

import { AppRoutingModule } from './/app-routing.module';
import { AppComponent } from './app.component';

// Encounter CRUD
import { EncounterService } from './encounter/shared/encounter.service';
import { EncounterListComponent } from './encounter/encounter-list/encounter-list.component';
import { EncounterDetailComponent } from './encounter/encounter-detail/encounter-detail.component';
import { EncounterFormComponent } from './encounter/encounter-form/encounter-form.component';


@NgModule({
  declarations: [
    AppComponent,
    EncounterListComponent,
    EncounterDetailComponent,
    EncounterFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AppRoutingModule,
  ],
  providers: [EncounterService],
  bootstrap: [AppComponent]
})
export class AppModule { }

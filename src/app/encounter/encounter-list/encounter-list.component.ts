import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Encounter } from '../shared/encounter';
import { EncounterService } from '../shared/encounter.service';

@Component({
  selector: 'app-encounter-list',
  templateUrl: './encounter-list.component.html',
  styleUrls: ['./encounter-list.component.css']
})
export class EncounterListComponent implements OnInit {

  encounters: Observable<Encounter[]>;

  constructor(private encounterService: EncounterService) { }

  ngOnInit() {
    this.getEncounters();    
  }

  getEncounters(): void {
    this.encounters = this.encounterService.getEncounters();
    this.encounters.subscribe(snapshot => {
      snapshot.forEach(obj => {
        console.log(obj);
      })
      
    });
  }

}

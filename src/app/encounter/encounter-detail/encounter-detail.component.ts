import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Encounter } from '../shared/encounter';
import { EncounterService } from '../shared/encounter.service';

@Component({
  selector: 'app-encounter-detail',
  templateUrl: './encounter-detail.component.html',
  styleUrls: ['./encounter-detail.component.css']
})
export class EncounterDetailComponent implements OnInit {
  
  encounter: Encounter;
  
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private encounterService: EncounterService
  ) { }

  ngOnInit(): void {
    this.getEncounter();
  }

  getEncounter(): void {
    const key = this.route.snapshot.paramMap.get('key');
    this.encounterService.getEncounter(key).subscribe(snapshot => {
      this.encounter = snapshot;
    });
  }

}

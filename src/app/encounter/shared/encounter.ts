export class Encounter {
    $key: string;
    name: string;
    active: boolean = false;   
}
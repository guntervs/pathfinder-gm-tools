import { Injectable } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireObject, AngularFireList } from 'angularfire2/database/interfaces';
import { Observable } from 'rxjs/Observable';

import { Encounter } from './encounter';

@Injectable()
export class EncounterService {

  private basePath: string = 'encounters/';

  private encounters: AngularFireList<Encounter>;
  private encounter: AngularFireObject<Encounter>;

  constructor(private db: AngularFireDatabase) { }

  getEncounters(): Observable<Encounter[]> {
    this.encounters = this.db.list<Encounter>(this.basePath);

    return this.encounters.valueChanges();
  }

  getEncounter(key: string): Observable<Encounter> {
    const path = `${this.basePath}/${key}`;
    this.encounter = this.db.object<Encounter>(path);
    return this.encounter.valueChanges();
  }

  addEncounter(encounter: Encounter): void {
    this.encounters.push(encounter);
  }
}

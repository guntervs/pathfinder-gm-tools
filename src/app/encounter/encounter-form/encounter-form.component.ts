import { Component, OnInit } from '@angular/core';

import { EncounterService } from '../shared/encounter.service';
import { Encounter } from '../shared/encounter';

@Component({
  selector: 'app-encounter-form',
  templateUrl: './encounter-form.component.html',
  styleUrls: ['./encounter-form.component.css']
})
export class EncounterFormComponent implements OnInit {
  private encounter: Encounter = new Encounter();
  constructor(private encounterService: EncounterService) { }

  ngOnInit() {

  }

  addEncounter(): void {
    this.encounterService.addEncounter(this.encounter);
    this.encounter = new Encounter();
  }

}

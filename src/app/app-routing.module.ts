import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EncounterListComponent } from './encounter/encounter-list/encounter-list.component' ;
import { EncounterDetailComponent } from './encounter/encounter-detail/encounter-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/admin/encounters', pathMatch: 'full' },
  { path: 'admin/encounters', component: EncounterListComponent },
  { path: 'admin/encounters/:key', component: EncounterDetailComponent },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule { }